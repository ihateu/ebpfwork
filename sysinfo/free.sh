#! /bin/sh

# ref: https://zhuanlan.zhihu.com/p/575366699
# calc free, ref: https://blog.csdn.net/younger_china/article/details/125686849
# ref: https://blog.csdn.net/weixin_45337360/article/details/125544275
# ref: https://blog.csdn.net/zgy666/article/details/110732036

info="proc-m.log"
cp /proc/meminfo $info

echo "-------------------------------------------------------------------------------  [free -k]"
free -k

echo "-------------------------------------------------------------------------------  [meminfo]"
MemTotal=$(grep "MemTotal:" $info | awk '{print $2}')
MemFree=$(grep "MemFree:"   $info | awk '{print $2}')
Cached=$(grep "^Cached:" $info | awk '{print $2}')
SReclaimable=$(grep "SReclaimable:" $info | awk '{print $2}')
Buffers=$(grep "Buffers:" $info | awk '{print $2}')
Shmem=$(grep "Shmem:" $info | awk '{print $2}')
MemAvailable=$(grep "MemAvailable:" $info | awk '{print $2}')
#Slab=$(grep "Slab:" $info | awk '{print $2}')

total=$MemTotal
used=$(($MemTotal - $MemFree - $Cached - $SReclaimable - $Buffers))
free=$MemFree
shared=$Shmem
buff_cache=$(($Buffers + $Cached + $SReclaimable))
available=$MemAvailable

printf "%19s %11s %11s %11s %11s %11s\n"  "total" "used" "free" "shared" "buff/cache" "available"
printf "%19s %11s %11s %11s %11s %11s\n"  $total  $used  $free  $shared  $buff_cache  $available
echo "-------------------------------------------------------------------------------"

