# trace-cmd

## debugfs
---
1. 进入debugfs目录 `cd /sys/kernel/debug/tracing` <br>
如果找不到则挂载debugfs目录：`mount -t debugfs nodev /sys/kernel/debug`

2. 查看支持的追踪器 `cat available_tracers` <br>
常用的追踪器有两种: `function` 和 `function_graph`


